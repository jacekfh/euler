import argparse

'''
   If we list all the natural numbers below 10 that are multiples of 3 or 5,
   we get 3, 5, 6 and 9. The sum of these multiples is 23.

   Find the sum of all the multiples of 3 or 5 below 1000.
'''


def brute_count(max_):
    suma = 0
    for i in xrange(1, max_):
        if not i % 3 or not i % 5:
            suma += i
    return suma


def clever_count(max_):

    def cnt(div):
        for high in range(max_ - 1, max_ - div - 1, -1):
            if not high % div:
                break
        diff = (high - div)
        avg = div + diff / 2.
        count = (1 + diff / div)
        suma = avg * count
        assert suma % 1 == 0
        return int(suma)

    return cnt(3) + cnt(5) - cnt(3 * 5)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--max", type=int, default=1000)
    parser.add_argument("--brute", action='store_true')
    args = parser.parse_args()

    if args.brute:
        print brute_count(args.max)
    else:
        print clever_count(args.max)
