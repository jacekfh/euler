import argparse
from primes import Primes

'''
   By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see
   that the 6^th prime is 13.

   What is the 10001^st prime number?
'''


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--max", type=int, default=10001)
    args = parser.parse_args()

    gen = Primes().gen()
    for i in xrange(args.max):
        curr = gen.next()
    print curr
