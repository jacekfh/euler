import math
import itertools


class Primes(object):
    def __init__(self):
        self.cache = [2, 3, 5, 7, 11, 13, 17, 19]

    def find_factor(self, number):
        max_factor = int(math.sqrt(int(number))) + 1
        for prime in self.gen(max_factor):
            if not number % prime:
                return prime
        return None

    def find_all_factors(self, number):
        factors = []
        while True:
            factor = self.find_factor(number)
            if not factor:
                if number > 1:
                    if number < 0xffffffff:
                        number = int(number & 0xffffffff)
                    factors.append(number)
                return factors
            factors.append(factor)
            number /= factor

    def is_prime(self, number):
        return not self.find_factor(number)

    def gen(self, max_=None):
        # Print already cached numbers
        for i in self.cache:
            if max_ and i > max_:
                return
            yield i
        # Calculate the rest
        for i in itertools.count(self.cache[-1] + 2, 2):
            if max_ and i > max_:
                return
            if self.find_factor(i) is None:
                self.cache.append(i)
                yield i
