import argparse
import math
import numpy as np
from primes import Primes

'''
   The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

   Find the sum of all the primes below two million.
'''


def calculate_primes(top):
    p = np.ones(top, dtype=np.bool)
    p[0], p[1] = False, False
    for i in xrange(int(math.sqrt(int(top))) + 2):
        if not p[i]:
            continue
        p[np.arange(2 * i, top, i)] = False
    return np.arange(top)[p].tolist()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--max", type=int, default=2000000)
    parser.add_argument("--brute", action='store_true')
    args = parser.parse_args()

    if args.brute:
        print sum(Primes().gen(args.max))
    else:
        print sum(calculate_primes(args.max))
