import argparse

'''
   The sum of the squares of the first ten natural numbers is,
                          1^2 + 2^2 + ... + 10^2 = 385

   The square of the sum of the first ten natural numbers is,
                       (1 + 2 + ... + 10)^2 = 55^2 = 3025

   Hence the difference between the sum of the squares of the first ten
   natural numbers and the square of the sum is 3025 385 = 2640.

   Find the difference between the sum of the squares of the first one
   hundred natural numbers and the square of the sum.
'''


def pow_sum(max_, slower=False):
    if slower:
        suma = sum(range(max_ + 1))  # array operations
    else:
        suma = int((max_ + 1.) * max_ / 2)  # arithmetic
    return pow(suma, 2)


def sum_pow(max_, slower=False):
    if slower:
        # whole array in memory
        return sum(map(lambda x: pow(x, 2), range(max_ + 1)))
    else:
        # generators, slightly faster
        suma = 0
        for x in xrange(max_ + 1):
            suma += pow(x, 2)
        return suma


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--max", type=int, default=100)
    parser.add_argument("--brute", action='store_true')
    args = parser.parse_args()

    x = pow_sum(args.max)
    y = sum_pow(args.max)
    print '{} - {} = {}'.format(x, y, x - y)
