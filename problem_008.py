import argparse

'''
   Find the greatest product of five consecutive digits in the 1000-digit
   number.
'''

BIG = (
    '73167176531330624919225119674426574742355349194934'
    '96983520312774506326239578318016984801869478851843'
    '85861560789112949495459501737958331952853208805511'
    '12540698747158523863050715693290963295227443043557'
    '66896648950445244523161731856403098711121722383113'
    '62229893423380308135336276614282806444486645238749'
    '30358907296290491560440772390713810515859307960866'
    '70172427121883998797908792274921901699720888093776'
    '65727333001053367881220235421809751254540594752243'
    '52584907711670556013604839586446706324415722155397'
    '53697817977846174064955149290862569321978468622482'
    '83972241375657056057490261407972968652414535100474'
    '82166370484403199890008895243450658541227588666881'
    '16427171479924442928230863465674813919123162824586'
    '17866458359124566529476545682848912883142607690042'
    '24219022671055626321111109370544217506941658960408'
    '07198403850962455444362981230987879927244284909188'
    '84580156166097919133875499200524063689912560717606'
    '05886116467109405077541002256983155200055935729725'
    '71636269561882670428252483600823257530420752963450'
)


def calc_product(digits):
    if isinstance(digits[0], int):
        return reduce(lambda x, y: x * y, digits)
    else:
        return reduce(lambda x, y: int(x) * int(y), digits)


def brute_search(window=5):
    cache = {}
    best = 0
    for idx in xrange(len(BIG) - window):
        digits = BIG[idx:idx + window]
        if digits in cache:
            continue
        cache[digits] = calc_product(digits)
        best = max(best, cache[digits])
    return best


def clever_search(window=5):
    best = 0
    biglist = [int(d) for d in BIG]
    move = None
    for idx in xrange(len(biglist) - window):
        if move is None:
            # No move: calculate whole product
            curr = calc_product(biglist[idx:idx + window])
        else:
            # With move: multiply by additional digit
            curr = move * biglist[idx + window - 1]
        best = max(best, curr)
        # Save common part for next iteration
        d = biglist[idx]
        if d == 0:
            move = None
        else:
            move = curr / d
    return best


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--repeat", type=int, default=1)
    parser.add_argument("--window", type=int, default=5)
    parser.add_argument("--brute", action='store_true')
    args = parser.parse_args()

    for i in xrange(args.repeat):
        if args.brute:
            result = brute_search(args.window)
        else:
            result = clever_search(args.window)
    print result
