import argparse

'''
   A palindromic number reads the same both ways. The largest palindrome made
   from the product of two 2-digit numbers is 9009 = 91 * 99.

   Find the largest palindrome made from the product of two 3-digit numbers.
'''


def brute_find_results(min_, max_):
    results = []
    for i in xrange(max_, min_ - 1, -1):
        for j in xrange(i, min_ - 1, -1):
            results.append(i * j)
    return sorted(results, reverse=True)


def brute_palindromes_gen(min_, max_, limit=None):
    results = brute_find_results(min_, max_)
    yielded = 0
    for x in results:
        if is_palindromic(x):
            yield x
            if limit:
                yielded += 1
                if yielded >= limit:
                    return


def find_results_gen(min_, max_):
    for i in xrange(max_, min_ - 1, -1):
        for j in xrange(max_, i - 1, -1):
            yield((i * j, j, i))


def find_max_palindrom(min_, max_):
    palindromes = []
    break_on = None
    for x, i, j in find_results_gen(min_, max_):
        if break_on and j < break_on:
            break
        if is_palindromic(x):
            palindromes.append(x)
            if not break_on:
                break_on = 1. * x / max_
    return sorted(palindromes, reverse=True)


def is_palindromic(number):
    text = str(number)
    for i in range(len(text) / 2):
        if text[i] != text[-i - 1]:
            return False
    else:
        return True


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--min", type=int, default=100)
    parser.add_argument("--max", type=int, default=999)
    parser.add_argument("--limit", type=int, default=10)
    parser.add_argument("--brute", action='store_true')
    args = parser.parse_args()

    if args.brute:
        for x in brute_palindromes_gen(args.min, args.max, args.limit):
            print x,
        print
    else:
        print find_max_palindrom(args.min, args.max)
