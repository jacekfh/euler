import argparse
import math

'''
   A Pythagorean triplet is a set of three natural numbers, a < b < c, for
   which,
                                a^2 + b^2 = c^2

   For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.

   There exists exactly one Pythagorean triplet for which a + b + c = 1000.
   Find the product abc.
'''


def find_c(a, b):
    c = math.sqrt(a * a + b * b)
    return c


def brute_search(number):
    for a in xrange(number / 2, 0, -1):
        for b in xrange(a, 0, -1):
            c = find_c(a, b)
            if a + b + c == number:
                return a, b, int(c)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--number", type=int, default=1000)
    args = parser.parse_args()

    result = brute_search(args.number)
    if result:
        a, b, c = result
        print '{} + {} + {} = {}'.format(a, b, c, args.number)
