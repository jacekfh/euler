import argparse
from primes import Primes

'''
   The prime factors of 13195 are 5, 7, 13 and 29.

   What is the largest prime factor of the number 600851475143 ?
'''

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--max-prime", type=int)
    parser.add_argument("--is-prime", type=int)
    parser.add_argument("--factors", type=int, default=600851475143)
    args = parser.parse_args()

    primes = Primes()
    if args.max_prime:
        print 'Primes up to {}:'.format(args.max_prime)
        for i in primes.gen(args.max_prime):
            print i,
        print
    if args.is_prime:
        factor = primes.find_factor(args.is_prime)
        if not factor:
            print '{} is a prime number'.format(args.is_prime)
        else:
            print '{} is not a prime number, having factor {}'.format(
                args.is_prime, factor)
    if args.factors:
        print 'Prime factors of {}:'.format(args.factors)
        print primes.find_all_factors(args.factors)
