import argparse

'''
   The following iterative sequence is defined for the set of positive
   integers:

   n n/2 (n is even)
   n 3n + 1 (n is odd)

   Using the rule above and starting with 13, we generate the following
   sequence:
                            13 40 20 10 5 16 8 4 2 1

   It can be seen that this sequence (starting at 13 and finishing at 1)
   contains 10 terms. Although it has not been proved yet (Collatz Problem),
   it is thought that all starting numbers finish at 1.

   Which starting number, under one million, produces the longest chain?

   NOTE: Once the chain starts the terms are allowed to go above one million.
'''


def check_length(start):
    chain_len = 1
    x = start
    while x > 1:
        if x & 1:
            x = 3 * x + 1
        else:
            x /= 2
        chain_len += 1
    return chain_len


cache = {}


def cached_check_length(x):
    global cache

    result = 1
    if x > 1:
        if x & 1:
            x2 = 3 * x + 1
        else:
            x2 = x / 2
        try:
            result += cache[x2]
        except KeyError:
            result += cached_check_length(x2)
    cache[x] = result
    return result


def cached_check_length_iter(start):
    global cache

    x = start
    result = 1
    new = [x]
    while x > 1:
        if x & 1:
            x = 3 * x + 1
        else:
            x /= 2
        try:
            result += cache[x]
            break
        except KeyError:
            result += 1
            new.append(x)
    for idx, x in enumerate(new):
        cache[x] = result - idx
    return result


def full_chain(start):
    chain = [start]
    x = start
    while x > 1:
        if x & 1:
            x = 3 * x + 1
        else:
            x /= 2
        chain.append(x)
    return chain


def find_longest(max_, cached=True):
    # Use cached version of check_length() if necessary
    if cached:
        check = cached_check_length
    else:
        check = check_length

    best = (0, 0)
    for i in xrange(1, max_):
        curr = check(i)
        if curr > best[1]:
            best = (i, curr)

    return best


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--max", type=int, default=1000000)
    parser.add_argument("--brute", action='store_true')
    parser.add_argument("--debug", action='store_true')
    args = parser.parse_args()

    if args.brute:
        print find_longest(args.max, cached=False)
    else:
        print find_longest(args.max)
        if args.debug:
            print 'unique: {}, max: {}'.format(len(cache), max(cache.keys()))
