import argparse
from collections import Counter
from primes import Primes

'''
   2520 is the smallest number that can be divided by each of the numbers
   from 1 to 10 without any remainder.

   What is the smallest number that is evenly divisible by all of the numbers
   from 1 to 20?
'''


def brute_find(max_):
    dividers = range(2, max_ + 1, 1)
    for i in xrange(2, 1000000000, 2):
        for d in dividers:
            if i % d:
                break
        else:
            return i


def clever_find(max_):
    dividers = range(2, max_ + 1, 1)
    primes = Primes()
    factors = Counter()
    for d in dividers:
        temp = Counter()
        for f in primes.find_all_factors(d):
            temp[f] += 1
        factors |= temp
    # print factors
    reduce1 = [pow(x, y) for x, y in factors.iteritems()]
    # print reduce1
    reduce2 = reduce(lambda x, y: x * y, reduce1)
    # print reduce2
    return reduce2


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--max", type=int, default=20)
    parser.add_argument("--brute", action='store_true')
    args = parser.parse_args()

    if args.brute:
        print brute_find(args.max)
    else:
        print clever_find(args.max)
